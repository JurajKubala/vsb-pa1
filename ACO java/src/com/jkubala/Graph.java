package com.jkubala;

import java.util.List;

class Graph {
    private double vaporizationParam = 0.5;
    private double qParam = 1;

    private final List<Place> places;
    private double[][] distance;
    private double[][] pheromones;

    Graph(List<Place> places) {
        this.places = places;
        distance = initDistanceMatrix(places);
        pheromones = initPheromoneMatrix(places);
    }

    void vaporize() {
        for (int i = 0; i < pheromones.length; i++) {
            for (int j = 0; j < pheromones.length; j++) {
                pheromones[i][j] *= (1 - vaporizationParam);
            }
        }
    }

    void updatePheromones(Ant ant) {
        List<Place> trail = ant.getTrail();
        Place a = trail.get(0);
        Place b = null;
        for (int i = 1; i < trail.size(); i++) {
            b = trail.get(i);
            pheromones[a.getId()][b.getId()] += qParam / ant.getTotalDistance();
            a = b;
        }
    }

    double getDistance(Place a, Place b) {
        return distance[a.getId()][b.getId()];
    }

    double getInverseDistance(Place a, Place b) {
        return getDistance(a, b) != 0 ? 1 / getDistance(a, b) : 0;
    }

    double getPheromone(Place a, Place b) {
        return pheromones[a.getId()][b.getId()];
    }

    int getPlacesCnt() {
        return places.size();
    }

    List<Place> getPlaces() {
        return places;
    }


    private double[][] initDistanceMatrix(List<Place> places) {
        double[][] matrix = new double[places.size()][places.size()];
        for (Place placeA : places) {
            for (Place placeB : places) {
                try {

                matrix[placeA.getId()][placeB.getId()] = placeA == placeB ? 0 : calculateDistance(placeA, placeB);
                }catch (Exception e) {
                    System.out.println("som tu");
                }
            }
        }
        return matrix;
    }

    private double[][] initPheromoneMatrix(List<Place> places) {
        double[][] matrix = new double[places.size()][places.size()];
        for (int i = 0; i < places.size(); i++) {
            for (int j = 0; j < places.size(); j++) {
                matrix[i][j] = 1;
            }
        }
        return matrix;
    }

    private double calculateDistance(Place a, Place b) {
        double dx = Math.pow(a.getX() - b.getX(), 2);
        double dy = Math.pow(a.getY() - b.getY(), 2);
        return Math.sqrt(dx + dy);
    }
}
