import numpy as np
import os
from copy import deepcopy
from shapes import addGlider, addGosperGliderGun

ALIVE = 1
DEAD = 0

class Grid:
    def __init__(self, m, n):
        self.m = m # rows (height)
        self.n = n # columns (width)
        self.grid = np.full((self.m, self.n), DEAD)
        self.nextGrid = deepcopy(self.grid)


    def addGlider(self, m = 1, n = 1):
        addGlider(m, n, self.grid)


    def addGosperGliderGun(self, m = 10, n = 10):
        addGosperGliderGun(m, n, self.grid)


    def addRandomPattern(self):
        self.grid = np.random.randint(2, size=(self.m, self.n))

    def step(self, startRow, endRow):
        for rowIdx in range(startRow, endRow):
            for colIdx in range(0, self.n):
                self.updateCell(rowIdx, colIdx)
                

    def updateCell(self, row, col):
        count = self.getAliveNeighbors(row, col)
        if self.grid[row][col] == ALIVE:
            # If a cell is ON and has either two or three neighbors that are ON, it remains ON.
            # If a cell is ON and has fewer than two neighbors that are ON, it turns OFF
            # If a cell is ON and has more than three neighbors that are ON, it turns OFF.
            if count == 2 or count == 3:
                self.nextGrid[row][col] = ALIVE
            else:
                self.nextGrid[row][col] = DEAD
        else:
            # If a cell is OFF and has exactly three neighbors that are ON, it turns ON.
            if count == 3:
                self.nextGrid[row][col] = ALIVE
            else:
                self.nextGrid[row][col] = DEAD
                

    def getAliveNeighbors(self, row, col):
        prevRow = row - 1 if (row - 1 >= 0) else self.m-1
        prevCol = col - 1 if (col - 1 >= 0) else self.n-1
        nextRow = row + 1 if (row + 1 < self.m) else 0
        nextCol = col + 1 if (col + 1 < self.n) else 0
        
        prevSum = self.grid[prevRow][prevCol] + self.grid[prevRow][col] + self.grid[prevRow][nextCol]
        currSum = self.grid[row][prevCol] + self.grid[row][nextCol]
        nextSum = self.grid[nextRow][prevCol] + self.grid[nextRow][col] + self.grid[nextRow][nextCol]

        return prevSum + currSum + nextSum


    def swapGrid(self):
        [self.grid, self.nextGrid] = [self.nextGrid, self.grid]

    def printGrid(self):
        result = '\n'.join(
            map(lambda row: ''.join(
                map(lambda cell: "X" if cell else " " , row)
            ), self.grid)
        )
        os.system('cls' if os.name=='nt' else 'clear')
        print(result)
        
