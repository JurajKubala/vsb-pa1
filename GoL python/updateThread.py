#!/usr/bin/python

import threading
import time
from grid import Grid

threadLock = threading.Lock()

class UpdateThread (threading.Thread):
    def __init__(self, grid, startRow, endRow):
        """
        :type grid: Grid
        """
        threading.Thread.__init__(self)

        self.grid = grid
        self.startRow = startRow
        self.endRow = endRow

    def run(self):
        self.grid.step(self.startRow, self.endRow)
