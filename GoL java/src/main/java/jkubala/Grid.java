package jkubala;

class Grid {
    static int ALIVE = 1;
    static int DEAD = 0;

    private int[][] grid;
    private int[][] nextGrid;
    private int rows;
    private int cols;

    Grid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = Utils.generateMatrix(rows, cols, DEAD);
        this.nextGrid = Utils.generateMatrix(rows, cols, DEAD);
    }

    void addGlider(int row, int col) {
        Utils.addGlider(this.grid, row, col);
    }

    void addGosperGliderGun(int row, int col) {
        Utils.addGosperGliderGun(this.grid, row, col);
    }

    void addRandomPattern() {
        this.grid = Utils.generateMatrix(this.rows, this.cols, a -> Math.random() < 0.5 ? DEAD : ALIVE);
    }

    void step(int startRow, int endRow) {
        for (int rowIdx = startRow; rowIdx < endRow; rowIdx++) {
            for (int colIdx = 0; colIdx < grid[rowIdx].length; colIdx++) {
                updateCell(rowIdx, colIdx);
            }
        }
    }

    private void updateCell(int row, int col) {

        int count = getAliveNeighbors(row, col);
        if (grid[row][col] == ALIVE) {
//          If a cell is ON and has either two or three neighbors that are ON, it remains ON.
//          If a cell is ON and has fewer than two neighbors that are ON, it turns OFF
//          If a cell is ON and has more than three neighbors that are ON, it turns OFF.
            if (count == 2 || count == 3) {
                nextGrid[row][col] = ALIVE;
            } else {
                nextGrid[row][col] = DEAD;
            }
        } else {
//          If a cell is OFF and has exactly three neighbors that are ON, it turns ON.
            if (count == 3) {
                nextGrid[row][col] = ALIVE;
            } else {
                nextGrid[row][col] = DEAD;
            }
        }
    }

    private int getAliveNeighbors(int row, int col) {
        int prevRow = (row - 1 >= 0) ? row - 1 : this.rows - 1;
        int prevCol = (col - 1 >= 0) ? col - 1 : this.cols - 1;
        int nextRow = (row + 1 < this.rows) ? row + 1 : 0;
        int nextCol = (col + 1 < this.cols) ? col + 1 : 0;

        int prevSum = grid[prevRow][prevCol] + grid[prevRow][col] + grid[prevRow][nextCol];
        int currSum = grid[row][prevCol] + grid[row][nextCol];
        int nextSum = grid[nextRow][prevCol] + grid[nextRow][col] + grid[nextRow][nextCol];

        return prevSum + currSum + nextSum;
    }

    void swapGrid() {
        int[][] tmp = this.grid;
        this.grid = this.nextGrid;
        this.nextGrid = tmp;
    }

    int getElement(int row, int col) {
        return grid[row][col];
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }
}
