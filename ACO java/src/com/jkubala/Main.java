package com.jkubala;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        int graphSize = 1000;
        int numberOfPlaces = 200;
        int numberOfAnts = 25;
        int iterations = 100;
        Random rand = new Random();

        // generate places
        List<Place> places = new ArrayList<>();
        for (int i = 0; i < numberOfPlaces; i++) {
            places.add(new Place(rand.nextDouble() * graphSize, rand.nextDouble() * graphSize));
        }

        //  execute with various number of threads
        //  Procesor
        //	Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz
        //	Základná rýchlosť:	2.90 GHz
        //	Jadrá:	2
        //	Logické procesory:	4

        run(graphSize, places, numberOfAnts, iterations, 1);
        // Serial: wandering 26137ms, evaporating 17ms, pheromoneupdate 1ms
        // Execution time: 26155ms

        run(graphSize, places, numberOfAnts, iterations, 2);
        // Parallel: wandering 12552ms, evaporating 0ms, pheromoneupdate 0ms
        // Execution time: 12552ms

        run(graphSize, places, numberOfAnts, iterations, 4);
        // Parallel: wandering 6348ms, evaporating 0ms, pheromoneupdate 0ms
        // Execution time: 6348ms
    }

    private static void run(int graphSize, List<Place> places, int numberOfAnts, int iterations, int threads) throws InterruptedException {
        final long startTime = System.currentTimeMillis();
        Random rand = new Random();

        Graph graph = new Graph(places);

        // init ants
        List<Ant> ants = new ArrayList<>();
        for (int i = 0; i < numberOfAnts; i++) {
            Place initialPlace = places.get(rand.nextInt(places.size()));
            ants.add(new Ant(initialPlace, graph));
        }

        if (threads > 1) {
            runParallel(iterations, graph, ants, threads);
        } else {
            runSerial(iterations, graph, ants);
        }

        final long elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Execution time: %sms \n", elapsedTime));
    }

    private static void runSerial(int iterations, Graph graph, List<Ant> ants) {
        long[] times = new long[3];
        for (int i = 0; i < iterations; i++) {
            long start = System.currentTimeMillis();
            for (Ant ant : ants) {
                ant.clear();
                while (ant.hasPlaceToMove())
                    ant.move();
                ant.closeTrail();
            }
            times[0] += System.currentTimeMillis() - start;

            start = System.currentTimeMillis();
            graph.vaporize();
            times[1] += System.currentTimeMillis() - start;

            start = System.currentTimeMillis();
            for (Ant ant : ants) {
                graph.updatePheromones(ant);
            }
            times[2] += System.currentTimeMillis() - start;
        }
        System.out.println(String.format("Serial: wandering %sms, evaporating %sms, pheromoneupdate %sms", times[0], times[1], times[2]));
    }

    private static void runParallel(int iterations, Graph graph, List<Ant> ants, int threadCnt) throws InterruptedException {
        long[] times = new long[3];
        for (int i = 0; i < iterations; i++) {

            long start = System.currentTimeMillis();

            // divide ants into equal groups
            int[][] intervals = Utils.createIntervals(ants.size() - 1, threadCnt);
            Thread[] threads = new Thread[threadCnt];
            for (int tidx = 0; tidx < threadCnt; tidx++) {
                int finalTidx = tidx;
                threads[tidx] = new Thread() {
                    @Override
                    public void run() {
                        for (Ant ant : ants.subList(intervals[finalTidx][0], intervals[finalTidx][1])) {
                            ant.clear();
                            while (ant.hasPlaceToMove())
                                ant.move();
                            ant.closeTrail();
                        }
                    }
                };
                threads[tidx].start();
            }

            // wait for all threads to be finnished
            for (Thread thread : threads)
                thread.join();
            times[0] += System.currentTimeMillis() - start;

            start = System.currentTimeMillis();
            graph.vaporize();
            times[1] += System.currentTimeMillis() - start;

            start = System.currentTimeMillis();
            for (Ant ant : ants) {
                graph.updatePheromones(ant);
            }
            times[2] += System.currentTimeMillis() - start;
        }
        System.out.println(String.format("Parallel: wandering %sms, evaporating %sms, pheromoneupdate %sms", times[0], times[1], times[2]));
    }


}
