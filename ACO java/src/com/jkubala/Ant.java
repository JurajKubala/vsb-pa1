package com.jkubala;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

class Ant {

    /**
     * Control params
     */
    final double alpha = 1;
    final double beta = 2;

    private List<Place> unvisitedPlaces;
    private Place currentPlace;
    private List<Place> trail;
    private double totalDistance = 0;
    private Graph graph;

    public Ant(Place initialPlace, Graph graph) {
        this.graph = graph;
        this.trail = new ArrayList<>();
        this.trail.add(initialPlace);
        this.currentPlace = initialPlace;

        this.unvisitedPlaces = new ArrayList<>(graph.getPlaces());

        this.clear();
    }

    void move() {

        // calculate attractivness
        double[] tmp = new double[this.unvisitedPlaces.size()];
        for (int i = 0; i < this.unvisitedPlaces.size(); i++) {
            Place unvisitedPlace = this.unvisitedPlaces.get(i);
            double p = Math.pow(this.graph.getPheromone(this.currentPlace, unvisitedPlace), this.alpha);
            double q = Math.pow(this.graph.getInverseDistance(this.currentPlace, unvisitedPlace), this.beta);
            tmp[i] = p * q;
        }
        double tmpSum = DoubleStream.of(tmp).sum();

        // calculate cumulative probabilities and choose next place
        double rand = Math.random();
        Place nextPlace = null;
        double[] probabilities = new double[this.unvisitedPlaces.size()];
        double cumulativeProb = 0;
        for (int i = 0; i < this.unvisitedPlaces.size(); i++) {
            Place unvisitedPlace = this.unvisitedPlaces.get(i);
            probabilities[i] = tmp[i] / tmpSum;
            cumulativeProb = DoubleStream.of(probabilities).sum();
            if (rand < cumulativeProb) {
                nextPlace = this.unvisitedPlaces.get(i);
                break;
            }
        }

        if (nextPlace != null) {
            totalDistance += graph.getDistance(currentPlace, nextPlace);
            currentPlace = nextPlace;
            trail.add(nextPlace);
            unvisitedPlaces.remove(nextPlace);
        }
    }

    void closeTrail() {
        Place initialPlace = trail.get(0);
        totalDistance += graph.getDistance(currentPlace, initialPlace);
        trail.add(initialPlace);
        currentPlace = initialPlace;
    }

    boolean hasPlaceToMove() {
        return graph.getPlacesCnt() > trail.size();
    }

    void clear() {
        trail = new ArrayList<>();
        trail.add(this.currentPlace);
        totalDistance = 0;

        unvisitedPlaces = new ArrayList<>(graph.getPlaces());
        unvisitedPlaces.remove(this.currentPlace);
    }

    List<Place> getTrail() {
        return trail;
    }

    double getTotalDistance() {
        return totalDistance;
    }
}
