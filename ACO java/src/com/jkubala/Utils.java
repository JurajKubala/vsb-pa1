package com.jkubala;

public class Utils {

    public static int[][] createIntervals(int n, int threads) {
        int[][] result = new int[threads][2];
        int size = (int) Math.ceil(n / (float)threads);
        int i = 0;
        for (int start = 0; start < n; start += size+1) {
            int end = Math.min(start + size, n);
            result[i][0] = start;
            result[i][1] = end;
            i++;
        }
        return result;
    }
}
