from grid import Grid
from updateThread import UpdateThread
import numpy as np
import matplotlib.pyplot as plt  
import matplotlib.animation as animation 
import datetime
import threading

startTime = datetime.datetime.now()
def log(msg):
    now = datetime.datetime.now() - startTime
    print("[{:3d}.{:6d}]: {}".format(now.seconds, now.microseconds, msg))

class GameOfLife:
    def __init__(self, iteration, rows, columns, showPlot, threads = 1):
        self.iteration = iteration
        self.isShowPlot = showPlot
        self.threads = threads

        self.grid = Grid(rows, columns)
        self.grid.addGlider(50, 1)
        self.grid.addGosperGliderGun(10, 10)
        self.grid.addRandomPattern()
        
    def run(self):
        if self.isShowPlot:
            self.showPlot()
        else:
            for _ in range(self.iteration):
                if self.threads == 1:
                    self.grid.step(0, self.grid.m)
                    self.grid.swapGrid()
                else:
                    # rodelit maticu po riadkoch
                    intervals = map(lambda arr: (min(arr), max(arr)), np.array_split(range(self.grid.m), self.threads))
                    threads = []
                    for interval in intervals:
                        t = UpdateThread(grid=self.grid, startRow=interval[0], endRow=interval[1])
                        t.start()
                        threads.append(t)

                    # Wait for all threads to complete
                    for t in threads: t.join()
                    self.grid.swapGrid()
                        


    def showPlot(self):
        # set up animation 
        fig, ax = plt.subplots() 
        img = ax.imshow(self.grid.grid, interpolation='nearest') 
        ani = animation.FuncAnimation(fig, self.updatePlot, fargs=(img, ), 
                                    frames = self.iteration, 
                                    interval = 10, 
                                    save_count = 50,
                                    repeat = False) 
        plt.show() 

    def updatePlot(self, _frame, img):
        self.grid.step(0, self.grid.m)
        img.set_data(self.grid.nextGrid)
        self.grid.swapGrid()
        return img, 

log("Game of Life: start")
GameOfLife(
    iteration = 5,
    rows = 500,
    columns = 500,
    showPlot = False,
    threads = 8
).run()
log("Game of Life: end")