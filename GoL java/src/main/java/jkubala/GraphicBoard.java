package jkubala;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GraphicBoard extends JPanel {

    public static int pixelSize = 5;
    private final Grid grid;

    public GraphicBoard(Grid g) {
        grid = g;
    }

    private void updateScreen(Graphics g) throws InterruptedException {
        BufferedImage buffered = new BufferedImage(grid.getCols() * pixelSize, grid.getRows() * pixelSize, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < grid.getRows(); i++) {
            for (int j = 0; j < grid.getCols(); j++) {

                int col = grid.getElement(i, j) == Grid.DEAD ? 0 : 65280;
                for (int k = 0; k < pixelSize; k++) {
                    for (int l = 0; l < pixelSize; l++) {
                        buffered.setRGB(j*pixelSize + k, i * pixelSize + l, col);
                    }
                }
            }
        }

        if (imageUpdate(buffered, ImageObserver.FRAMEBITS, 0, 0, buffered.getWidth() * pixelSize, buffered.getHeight() * pixelSize))
            g.drawImage(buffered, 0, 0, Color.BLACK, null);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        try {
            updateScreen(g);
        } catch (InterruptedException ex) {
            Logger.getLogger(GraphicBoard.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
}
