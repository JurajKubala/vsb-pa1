package com.jkubala;

public class Place {
    private static int globalId = 0;
    private double x;
    private double y;
    private int id;

    public Place(double x, double y) {
        this.x = x;
        this.y = y;
        this.id = globalId++;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getId() {
        return id;
    }
}
