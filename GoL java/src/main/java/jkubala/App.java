package jkubala;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.*;

import static jkubala.GraphicBoard.pixelSize;

public final class App {

    public static void main(String[] args) throws InterruptedException {

        int iterations = 100;
        int rows = 5000;
        int cols = rows;
        boolean graphics = false;

        // ## execute GoL with various number of threads
        //  Procesor
        //	Intel(R) Core(TM) i7-7500U CPU @ 2.70GHz
        //	Základná rýchlosť:	2.90 GHz
        //	Jadrá:	2
        //	Logické procesory:	4
        run(iterations, rows, cols, 1, graphics); // Execution time with 1 threads: 8738ms
        run(iterations, rows, cols, 2, graphics); // Execution time with 2 threads: 5345ms
        run(iterations, rows, cols, 4, graphics); // Execution time with 4 threads: 4485ms
    }

    private static void run(int iterations, int rows, int cols, int threadCnt, boolean graphics) throws InterruptedException {
        final long startTime = System.currentTimeMillis();

        Grid grid = new Grid(rows, cols);
        grid.addGlider(30, 1);
        grid.addGosperGliderGun(33, 33);

        if (graphics) {
            showGraph(rows, cols, grid);
        }

        if (threadCnt > 1) {
            parallelExec(iterations, rows, threadCnt, grid);
        } else {
            serialExec(iterations, rows, grid);
        }

        final long elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println(String.format("rows: %s, cols: %s, iterations: %s, threads: %s", rows, cols, iterations, threadCnt));
        System.out.println(String.format("Execution time: %sms \n", elapsedTime));
    }

    private static void serialExec(int iterations, int rows, Grid grid) {
        for (int i = 0; i < iterations; i++) {
            grid.step(0, rows);
            grid.swapGrid();
        }
    }

    private static void parallelExec(int iterations, int rows, int threadCnt, Grid grid) throws InterruptedException {
        for (int iter = 0; iter < iterations; iter++) {

            // divide matrix to equal groups of rows
            int[][] intervals = Utils.createIntervals(rows, threadCnt);
            Thread[] threads = new Thread[threadCnt];
            for (int i = 0; i < threadCnt; i++) {
                int finalI = i;

                // run grid.step in separate threads
                threads[i] = new Thread(() -> grid.step(intervals[finalI][0], intervals[finalI][1]));
                threads[i].start();
            }

            // wait for all threads to be finnished
            for (Thread thread : threads) {
                thread.join();
            }

            // swap new calculated grid
            grid.swapGrid();
        }
    }

    private static void showGraph(int rows, int cols, Grid grid) {
        JFrame frame = new JFrame("Game of Life");
        Graphics g = frame.getGraphics();
        frame.pack();
        Insets insets = frame.getInsets();
        GraphicBoard gb = new GraphicBoard(grid);
        frame.getContentPane().add(gb, BorderLayout.CENTER);
        frame.paint(g);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(insets.left + insets.right + cols * pixelSize, insets.top + insets.bottom + rows * pixelSize);
        frame.setVisible(true);
    }

}
