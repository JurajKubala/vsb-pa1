package jkubala;

import java.util.function.Function;

public class Utils {

    public static int[][] generateMatrix(int row, int col, Function<Void, Integer> func) {
        int[][] matrix = new int[row][col];
        for (int rowIdx = 0; rowIdx < matrix.length; rowIdx++) {
            for (int colIdx = 0; colIdx < matrix[rowIdx].length; colIdx++) {
                matrix[rowIdx][colIdx] = func.apply(null);
            }
        }
        return matrix;

    }

    public static int[][] generateMatrix(int row, int col, int value) {
        return generateMatrix(row, col, a -> value);
    }

    public static void addGlider(int[][] matrix, int row, int col) {
        String[] pattern = new String[]{
            "__X",
            "X_X",
            "_XX"
        };

        applyPattern(matrix, row, col, pattern);
    }

    public static void addGosperGliderGun(int[][] matrix, int row, int col) {
        String[] pattern = new String[]{
            "______________________________________",
            "_________________________X____________",
            "_______________________X_X____________",
            "_____________XX______XX____________XX_",
            "____________X___X____XX____________XX_",
            "_XX________X_____X___XX_______________",
            "_XX________X___X_XX____X_X____________",
            "___________X_____X_______X____________",
            "____________X___X_____________________",
            "_____________XX_______________________",
            "______________________________________"
        };

        applyPattern(matrix, row, col, pattern);
    }

    private static void applyPattern(int[][] matrix, int row, int col, String[] pattern) {
        for (int i = 0; i < pattern.length; i++) {
            for (int j = 0; j < pattern[i].split("").length; j++) {
                matrix[row + i + 1][col + j + 1] = pattern[i].charAt(j) == 'X' ? Grid.ALIVE : Grid.DEAD;
            }
        }
    }

    public static int[][] createIntervals(int rows, int threads) {
        int[][] result = new int[threads][2];
        int size = (int) Math.ceil(rows / (float)threads);
        int i = 0;
        for (int start = 0; start < rows; start += size+1) {
            int end = Math.min(start + size, rows);
            result[i][0] = start;
            result[i][1] = end;
            i++;
        }
        return result;
    }

}
